# aircon

Use a Raspberry Pi to control an airconditioner

These functions are provided by the RPi.
## MANUAL
The RPi does not control the airco. Instead the user controls the airco directly using the user-remote.
The RPi tries to keep up with setpoint changes by monitoring the IR sensor.

## SEMI-AUTO:OFF
The airco is switched to `off mode`.

## SEMI-AUTO:COOL
In this mode the temperature setpoint is used to cool the room.

|  T(PV-SP) | `FAN` | `MODE` |
|:------:|:---:|:---|
| < -5   | `OFF` | `OFF`  |
| < -2   | `1` |  `FAN` |
| -2..+2 | `1`| `COOL` |
| \> +2   | `2`| `COOL` |
| \> +5   | `3`| `COOL` |

## SEMI-AUTO:HEAT
In this mode the temperature setpoint is used to heat the room.

| T(PV-SP) | `FAN` | `MODE` |
|:------:|:---:|:---|
| < -5   | `3`| `HEAT` |
| < -2   | `2`| `HEAT` |
| -2..+2 | `1`| `HEAT` |
| \> +2   | `1` | `FAN` |
| \> +5   | `OFF` | `OFF` |

## SEMI-AUTO:DRY
In this mode the airco is switched to `dry mode`. When the humidity setpoint is reached the RPi changes the temperature
setpoint to the current temperature and then switches to SEMI-AUTO:AUTO mode.

|  RH(PV-SP) | `FAN` | `MODE` |
|:------:|:---:|:---|
| < +0   | | `AUTO` |
| < +2   | `1` | `DRY` |
| \> +2  | `2` | `DRY` |
| \> +5  | `3` | `DRY` |

## SEMI-AUTO:AUTO
