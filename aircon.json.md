# aircon.json configuration file

The airconXX.json configuration file is used to store data from various 


```json5
{
  "time": 16000000,
  "t_adjust": {  // gain and offset required to correct average temperature result from BMP and SHT
              "temperature_gain": 1.0,
              "temperature_offset": 0.0
              },
  "sensors": {
              // this holds recent telemetry data {mean, samples[]}
              "temperature": {'mean': 25.5, 'samples':[],  // average temperature by BMP085 and SHT31
                              'gain': 1.0, 'offset': 0.0 }, // correction factors to adjust sensor-data to fit AC-data
              "temperature_ac": {'mean': 20.0, 'samples':[] },       // temperature as reported by the AC
              "temperature_outside": {'mean': 20.3, 'samples':[] },  // outside temperature measured by the AC
              "humidity": {'mean': 54.0, 'samples':[] },  // humidity measured by SHT31
              "pressure": {'mean': 1012.3, 'samples':[] }, // pressure measured by BMP085
              "co2": {'mean': 456, 'samples':[] }, // eCO2 measured by CCS811
              "voc": {'mean': 18, 'samples':[] }  // tVOC measured by CCS811
             },
  "control": {
              // this reflects the current (control) state of the AC
              "null": true
             },
  "request": {
              // this reflects the desired (changes to the)
              // control state of the AC
              "null": false
             }
}
```
