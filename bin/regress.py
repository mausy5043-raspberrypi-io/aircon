#!/usr/bin/env python3

"""Calculate linear regression for temperature measurements."""

import argparse
import os

import scipy.stats as st

import acgraphlib as alib
import constants

HERE = os.path.realpath(__file__).split('/')
MYROOT = "/".join(HERE[0:-3])
# central store
CONFIG = f'{MYROOT}/.config/airconf.json'

DATABASE = constants.AC['database']

parser = argparse.ArgumentParser(description="Determine line fitting parameters between two sensors",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-d',
                    '--days',
                    type=int,
                    default=6,
                    help='number of days of data to use for the fit'
                    )
parser.add_argument('-x',
                    '--x',
                    type=str,
                    default='th',
                    help='select sensor for x-data: sht, bmp, th, outside or ac'
                    )
parser.add_argument('-y',
                    '--y',
                    type=str,
                    default='ac',
                    help='select sensor for y-data: sht, bmp, th, outside or ac'
                    )
parser.add_argument('-p',
                    '--power',
                    dest='power',
                    action='store_true',
                    help='only use data when AC is swtiched on'
                    )
parser.add_argument('-n',
                    '--no-power',
                    dest='power',
                    action='store_false',
                    help='disregard AC powered state'
                    )
# parser.add_argument('--save', action='store_true', help='save fitting parameters for later use')
OPTION = parser.parse_args()

# fetch x and y data from database
config = alib.add_time_line({'grouping': '',
                             'period': OPTION.days * 24,
                             'timeframe': 'hour',
                             'database': DATABASE,
                             'table': 'aircon'
                             })

parameter_x = ''.join(['temperature', '_', OPTION.x])
parameter_y = ''.join(['temperature', '_', OPTION.y])

parameter_extra_where = ''
if OPTION.power:
    parameter_extra_where = 'ac_power = 1'

t_x, _ = alib.get_historic_data(config, parameter=parameter_x, extra_where=parameter_extra_where)
t_y, data_lbls = alib.get_historic_data(config, parameter=parameter_y, extra_where=parameter_extra_where)

coefficients_xy = st.linregress(t_x, t_y)
coefficients_yx = st.linregress(t_y, t_x)
print(f"Fitting parameters    x->y: {coefficients_xy.slope:.4f}   {coefficients_xy.intercept:.4f}"
      f"   y->x: {coefficients_yx.slope:.4f}   {coefficients_yx.intercept:.4f}")
