#!/usr/bin/sqlite3
# SQLite3 script
# version: 2
# create tables for storage of room environmental data

DROP TABLE IF EXISTS aircon;

CREATE TABLE aircon (
    sample_time         datetime NOT NULL,
    sample_epoch        integer NOT NULL,
    ac_power            integer,
    ac_mode             integer,
    temperature_sht     real,
    temperature_bmp     real,
    temperature_th      real,
    temperature_ac      real,
    temperature_target  real,
    temperature_outside real,
    humidity_sht        real,
    humidity_th         real,
    pressure            real,
    voc                 real,
    co2                 real,
    cmp_freq            integer
    );

CREATE INDEX idx_time ON aircon(sample_time);
CREATE INDEX idx_epoch ON aircon(sample_epoch);
