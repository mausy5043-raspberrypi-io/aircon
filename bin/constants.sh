app_name="aircon"

# determine controller's identity
host_name=$(hostname)
room_id="${host_name: -2}"

# construct database paths
database_filename="${app_name}${room_id}.v2.sqlite3"
database_path="/srv/databases"
db_full_path="${database_path}/${database_filename}"
