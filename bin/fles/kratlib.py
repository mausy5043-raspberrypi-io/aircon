#!/usr/bin/env python3

"""Common functions for Flask webUI"""

import json
import os
import sys
import sqlite3
import time

sys.path.insert(0, os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
import constants    # noqa


def initial_state():
    """Define initial state

    Returns:
        dict: values for various settings
    """
    defstate = dict()
    """OPERATOR :

       -1 : MANUAL control; user directly controls the airco; the app does nothing.
        0 : SEMI-AUTO control; user controls the airco via the application
        1 : AUTO;
        2 : REMOTE control; the application is in control of the airco

        MODE_SP :
       -1 : OFF
        0 : AUTO
        1 : COOL
        2 : DRY
        3 : HEAT
        4 : FAN ONLY
    """
    defstate['OPERATOR'] = -1
    defstate['Mode_SP'] = -1
    defstate['Temperature_SP'] = 20.0
    defstate['Temperature_dSP'] = 0.5
    defstate['Humidity_SP'] = 50.0
    defstate['Humidity_dSP'] = 0.5
    defstate['Fan_SP'] = 1
    defstate['Fan_dSP'] = 1
    return defstate


class Fles:
    """Class for the fles Flask
    """

    def __init__(self):
        # app info :
        self.HERE = os.path.realpath(__file__).split('/')  # path to this file as a list of elements
        self.MYLEVEL = 4  # aircon =1, bin =2, fles =3
        # self.MYAPP = self.HERE[-self.MYLEVEL]  # element that contains the appname (given the location of this file)
        self.MYROOT = "/".join(self.HERE[0:-self.MYLEVEL])  # absolute path to the app's root
        # self.NODE = os.uname()[1]  # name of the host
        self.ROOM_ID = constants.AC['room_id']

        self.AIRCO_IP = constants.AC['ip_airco']
        self.DATABASE = constants.AC['database']
        self.CONFIG = f'{self.MYROOT}/.config/airconf.json'
        self.req_state = dict()
        self.ctrl_state = dict()
        self.load_state()

    def get_latest_data(self, fields):
        """Retrieve the most recent datapoints from the database.
        """
        db_con = sqlite3.connect(self.DATABASE)
        with db_con:
            db_cur = db_con.cursor()
            db_cur.execute(f"SELECT {fields} FROM aircon \
                             WHERE sample_epoch = (SELECT MAX(sample_epoch) FROM aircon) \
                             ;"
                           )
            db_data = db_cur.fetchall()
        return list(db_data[0])

    def set(self, key, value):
        """Store the key-value pair
        """
        self.req_state[key] = value
        # immediately save the new state
        self.save_state()

    def get(self, key):
        """Return the value of the key
        """
        #  Loading the state every time a parameter is needed
        #  might enable adjusting settings via the terminal
        #  on the fly.
        self.load_state()
        return self.req_state[key]

    def get_ctrl(self, key):
        """Interrogate the state of the <key>

        Args:
            key (string):  a valid control key

        Returns:
            string: value of the <key>
        """
        #  Loading the state every time a parameter is needed
        #  might enable adjusting settings via the terminal
        #  on the fly.
        self.load_state()
        return self.ctrl_state[key]

    def get_ctrl_state(self):
        """Get the control state

        Returns:
            dict: control state
        """
        self.load_state()
        return self.ctrl_state

    def save_state(self):
        """Save the settings to disk.
        """
        nosj_data = dict()
        if os.path.isfile(self.CONFIG):
            with open(self.CONFIG, 'r') as fp:
                nosj_data = json.load(fp)
        nosj_data['request'] = self.req_state
        # update the timestamp
        nosj_data['time'] = time.time()
        with open(self.CONFIG, 'w') as fp:
            json.dump(nosj_data, fp, sort_keys=True, indent=4)

    def load_state(self):
        """Load the factory settings, then combine them with a local configurationfile (if exists).
        """
        nosj_data = dict()
        defaults = initial_state()
        if os.path.isfile(self.CONFIG):
            with open(self.CONFIG, 'r') as fp:
                nosj_data = json.load(fp)
                try:
                    self.req_state = nosj_data['request']
                except KeyError:
                    nosj_data['request'] = {}  # this might be wrong
                    pass
                try:
                    self.ctrl_state = nosj_data['control']
                except KeyError:
                    nosj_data['control'] = {}  # this might be wrong
                    pass
        # add missing defaults
        for element in defaults:
            if element not in self.req_state:
                self.req_state[element] = defaults[element]
