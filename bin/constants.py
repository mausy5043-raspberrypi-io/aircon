#!/usr/bin/env python3

import os
import sys

_MYHOME = os.environ["HOME"]
_NODE = os.uname()[1]
_ROOM_ID = _NODE[-2:]
_DB_FILE = f'aircon{_ROOM_ID}.v2.sqlite3'
_DATABASE = f'/srv/databases/{_DB_FILE}'
_IP_AIRCO = '0.0.0.0'

if _ROOM_ID == '01':
    _IP_AIRCO = '192.168.2.30'
if _ROOM_ID == '11':
    _IP_AIRCO = '192.168.2.31'
if _IP_AIRCO == '0.0.0.0':
    print(f"Unknown room {_ROOM_ID} for host {_NODE}")
    sys.exit(1)


if not os.path.isfile(_DATABASE):
    _DATABASE = f'/srv/data/{_DB_FILE}'
if not os.path.isfile(_DATABASE):
    _DATABASE = f'/mnt/data/{_DB_FILE}'
if not os.path.isfile(_DATABASE):
    _DATABASE = f'.local/{_DB_FILE}'
if not os.path.isfile(_DATABASE):
    _DATABASE = f'{_MYHOME}/.sqlite3/{_DB_FILE}'

AC = {'database': _DATABASE,
      'ip_airco': _IP_AIRCO,
      'room_id': _ROOM_ID,
      'sql_command': "INSERT INTO aircon ("
                     "sample_time, sample_epoch, "
                     "ac_power, ac_mode,"
                     "temperature_sht, temperature_bmp, temperature_th, "
                     "temperature_ac, temperature_target, temperature_outside, "
                     "humidity_sht, humidity_th, "
                     "pressure, "
                     "voc, co2, "
                     "cmp_freq) "
                     "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
      'sql_table': "aircon",
      'report_time': 60,
      'samplesperreport': 3,
      'gracetime': 1,
      'cycles': 3,
      'samplespercycle': 5,
      'scan_time': 58
      }
