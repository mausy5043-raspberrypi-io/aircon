#!/usr/bin/env python3

"""Sensor support functions"""

import random
import time

import numpy as np
from Adafruit_BMP.BMP085 import *
from Adafruit_CCS811 import Adafruit_CCS811  # noqa
from Adafruit_SHT31 import *

from temperhumlib import Temper

PI = np.pi
random.seed()
try:
    SHT_SENSOR = SHT31()
except:
    SHT_SENSOR = None
try:
    BMP_SENSOR = BMP085()
except:
    BMP_SENSOR = None
try:
    CCS_SENSOR = Adafruit_CCS811()
except:
    CCS_SENSOR = None
try:
    TEMPERHUM_SENSOR = Temper()
except:
    TEMPERHUM_SENSOR = None

while CCS_SENSOR and not CCS_SENSOR.available():
    pass


def generate_signal(seed):
    """generate a randomized sine-signal for testing

    Args:
        seed (float): startingpoint for sine to be used

    Yields:
        float: a number that kinda follows a sine-curve
    """
    global PI
    delta = PI / 3691.
    while True:
        yield np.sin(seed) + random.random() * 0.2
        seed += delta


GEN = generate_signal(int(time.time()))


def get_sht_data():
    """Retrieve current temperature and humidity from SHT31-device

    Returns:
        dict: temperature [degC] and humidity [%]
    """
    global SHT_SENSOR
    sht_temperature = sht_humidity = np.nan
    try:
        if SHT_SENSOR:
            sht_temperature = SHT_SENSOR.read_temperature()
            sht_humidity = SHT_SENSOR.read_humidity()
    except OSError:
        pass
    return {'temperature_sht': sht_temperature, 'humidity_sht': sht_humidity}


def get_bmp_data():
    """Retrieve current temperature and pressure from BMP085 device

    Returns:
        dict: temperature [degC] and air pressue [mbar-abs]
    """
    global BMP_SENSOR
    bmp_temperature = bmp_pressure = np.nan
    # convert [Pa] to [mbara]
    try:
        if BMP_SENSOR:
            bmp_pressure = float(BMP_SENSOR.read_pressure()) / 100
            bmp_temperature = BMP_SENSOR.read_temperature()
    except OSError:
        pass
    return {'temperature_bmp': bmp_temperature, 'pressure_bmp': bmp_pressure}


def get_tmphm_data():
    """Retrieve current temperature and humidity from connected TEMPerHUM-devices

    Returns:
        dict: temperature [degC] and relative humidity [%]
    """
    global TEMPERHUM_SENSOR
    device_data = dict()
    result_data = dict()
    inter_t = np.nan
    inter_h = np.nan
    inter_bus = np.nan
    inter_dev = np.nan
    if TEMPERHUM_SENSOR:
        result_data = TEMPERHUM_SENSOR.read()
    for device in result_data:
        for k, v in device.items():
            if 'temperature' in k:
                inter_t = v
            if 'humidity' in k:
                inter_h = v
            if 'busnum' in k:
                inter_bus = v
            if 'devnum' in k:
                inter_dev = v
        i = str(int(inter_bus) + int(inter_dev) / 100)
        device_data = {f'temperature_{i}': inter_t,
                       f'humidity_{i}': inter_h
                       }
    return device_data


def get_ccs_qdata():
    """Retieve air quality data

    Returns:
        dict: Total VOC concentration [ppb] and CO2 concentration [ppm]
    """
    global CCS_SENSOR
    # set default values for sensor incase it fails to return a value
    ccs_voc = np.nan
    ccs_co2 = np.nan
    if CCS_SENSOR and CCS_SENSOR.available():
        # localT, localH = getTRH()
        # this is not working
        # CCS_SENSOR.setEnvironmentalData(localH, localT)
        time.sleep(0.5)
        if not CCS_SENSOR.readData():
            ccs_voc = CCS_SENSOR.getTVOC()
            ccs_co2 = CCS_SENSOR.geteCO2()
    return {'voc_ccs': ccs_voc, 'co2_ccs': ccs_co2}
