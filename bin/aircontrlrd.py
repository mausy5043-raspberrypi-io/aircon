#!/usr/bin/python3 -u

"""
Interact with the configured AC.
"""

import argparse
import json
import os
import time
import traceback

import mausy5043libs.libsignals3 as ml

import constants
from fles import daikinlib

parser = argparse.ArgumentParser(description="Execute the AC controller daemon.")
parser_group = parser.add_mutually_exclusive_group(required=True)
parser_group.add_argument('--start',
                          action='store_true',
                          help='start the daemon as a service')
parser_group.add_argument('--debug',
                          action='store_true',
                          help='start the daemon in debugging mode')
OPTION = parser.parse_args()

# constants
DEBUG = False
HERE = os.path.realpath(__file__).split('/')
# runlist id :
# MYID = HERE[-1]
# app_name :
# MYAPP = HERE[-3]
MYROOT = "/".join(HERE[0:-3])
# host_name :
# NODE = os.uname()[1]

# example values:
# HERE: ['', 'home', 'pi', 'aircon', 'bin', 'aircontrlrd.py']
# MYID: 'aircontrlrd.py
# MYAPP: aircon
# MYROOT: /home/pi
# NODE: rbair11
# ROOM_ID: 11

# central store
CONFIG = f'{MYROOT}/.config/airconf.json'

# connect to AC
AIRCO_IP = '0.0.0.0'
AIRCO = daikinlib.Daikin(AIRCO_IP)

# set defaults
DATA_STORE = {"adv": "",
              "alert": "255",
              "cmpfreq": "0",
              "err": "0",
              "f_dir": "0",
              "f_rate": "A",
              "hhum": "-",
              "htemp": "18.0",
              "mode": "4",
              "otemp": "-4.0",
              "pow": "0",
              "ret": "OK",
              "shum": "0",
              "stemp": "17.0"
              }


# These we don't need.
#              'b_mode': '6',
#              'b_stemp': '--', 'b_shum': '--',
#              'b_f_rate': '5',
#              'b_f_dir': '3',
#              "dfdh": "0",
#              "dfrh": "5",


def main():
    """Execute main loop."""
    global CONFIG
    global DEBUG
    global DATA_STORE
    global AIRCO
    global AIRCO_IP
    killer = ml.GracefulKiller()
    scan_period = constants.AC['scan_time']
    AIRCO_IP = constants.AC['ip_airco']

    AIRCO = daikinlib.Daikin(AIRCO_IP)
    # logging will fail if airco is not present
    print(f"{AIRCO.__str__()}")

    # load state
    DATA_STORE = load_state(CONFIG, DATA_STORE)

    pause_time = time.time()
    while not killer.kill_now:
        if time.time() > pause_time:
            start_time = time.time()
            try:
                do_work()

            except OSError:
                if DEBUG:
                    print(traceback.format_exc())
                    print("The above error will be passed.")
                # pass
            except Exception:
                print("Unexpected error in run()")
                print(traceback.format_exc())
                print("The above error will be raised. Please pick up the pieces.")
                raise

            pause_time = (scan_period
                          - (time.time() - start_time)
                          - (start_time % scan_period)
                          + time.time())
            if pause_time > 0 and DEBUG:
                print(f"Waiting  : {pause_time - time.time():.1f}s")
                print("................................")
            if pause_time < 0 and DEBUG:
                print(f"Behind   : {pause_time - time.time():.1f}s")
                print("................................")
        time.sleep(1.0)


def do_work():
    """do some work"""
    global AIRCO
    global DATA_STORE
    global DEBUG
    store_changed = False
    result = dict()

    retries = 3
    while retries > 0:
        try:
            result = AIRCO.all_sensor_fields()
            break
        except ConnectionError:
            print("AC ConnectionError")
            time.sleep(10)
            retries -= 1
    if DEBUG:
        print(f"Sensor data  : {result}")
    # update data
    for element in result:
        if element in DATA_STORE:
            if DATA_STORE[element] != result[element]:
                store_changed = True
                DATA_STORE[element] = result[element]

    retries = 3
    while retries > 0:
        try:
            result = AIRCO.all_control_fields()
            break
        except ConnectionError:
            print("AC ConnectionError")
            time.sleep(10)
            retries -= 1
    if DEBUG:
        print(f"Control data : {result}")
    # update data
    for element in result:
        if element in DATA_STORE:
            if DATA_STORE[element] != result[element]:
                store_changed = True
                DATA_STORE[element] = result[element]
    if store_changed:
        if DEBUG:
            print(f"Data changed : {DATA_STORE}")
        save_state()


def save_state():
    """Save the settings to disk."""
    global DEBUG
    global CONFIG
    global DATA_STORE
    nosj_data = dict()
    # first load the settings!
    if os.path.isfile(CONFIG):
        with open(CONFIG, 'r') as fp:
            nosj_data = json.load(fp)
    # insert current state
    nosj_data['control'] = DATA_STORE
    # update the timestamp
    nosj_data['time'] = time.time()
    # and write the file back
    if DEBUG:
        print("Saving state.")
    with open(CONFIG, 'w') as fp:
        json.dump(nosj_data, fp, sort_keys=True, indent=4)


def load_state(from_file, defaults):
    """Combine defaults with a local configurationfile (if exists)."""
    global DEBUG
    nosj_data = dict()
    if DEBUG:
        print("Loading state.")
    if os.path.isfile(from_file):
        with open(from_file, 'r') as fp:
            nosj_data = json.load(fp)
            try:
                nosj_data = nosj_data['control']
            except KeyError:
                nosj_data = {}
                # pass
    # add missing defaults
    for element in defaults:
        if element not in nosj_data:
            nosj_data[element] = defaults[element]
    return nosj_data


if __name__ == "__main__":
    if OPTION.debug:
        DEBUG = True
        print("Debug-mode started.")
        print("Use <Ctrl>+C to stop.")
        main()

    if OPTION.start:
        main()

    print("And it's goodnight from him")
