#!/usr/bin/python3 -u
"""
Gather data from the (connected) environmental sensors.

- temperature
- humidity
- pressure
- total VOCs
- equivalent CO2 concentration
- outside and inside temperature by airco

Store the data in an sqlite3 database.
"""

import argparse
import datetime as dt
import json
import os
import sqlite3
import time
import traceback

# import mausy5043funcs.fileops3 as mf
import mausy5043libs.libsignals3 as ml
import numpy as np

import constants
import sensorlib as asens
from fles import daikinlib

from hanging_threads import start_monitoring
anti_freeze = constants.AC['report_time'] * 2

parser = argparse.ArgumentParser(description="Execute the telemetry daemon.")
parser_group = parser.add_mutually_exclusive_group(required=True)
parser_group.add_argument('--start', action='store_true', help='start the daemon as a service')
parser_group.add_argument('--debug', action='store_true', help='start the daemon in debugging mode')
OPTION = parser.parse_args()

# constants
DEBUG = False
HERE = os.path.realpath(__file__).split('/')
# runlist id :
# MYID = HERE[-1]
# app_name :
MYAPP = HERE[-3]
MYROOT = "/".join(HERE[0:-3])
# host_name :
# NODE = os.uname()[1]
# inferred roomnumber
# ROOM_ID = NODE[-2:]

# example values:
# HERE: ['', 'home', 'pi', 'aircon', 'bin', 'sensaird.py']
# MYID: 'sensaird.py
# MYAPP: aircon
# MYROOT: /home/pi
# NODE: rbair11
# ROOM_ID: 11

# central store
CONFIG = f"{MYROOT}/.config/airconf{constants.AC['room_id']}.json"

AIRCO_IP = '0.0.0.0'
AIRCO = daikinlib.Daikin(AIRCO_IP)

# set defaults
DATA_STORE = {
    'temperature': {'gain': 1.0, 'offset': 0.0},
    'temperature_sht': {'now': np.nan, 'mean': 0.0, 'samples': []},
    'temperature_bmp': {'now': np.nan, 'mean': 0.0, 'samples': []},
    'temperature_th': {'now': np.nan, 'mean': 0.0, 'samples': []},
    'temperature_ac': {'now': np.nan, 'mean': 0.0, 'samples': []},
    'temperature_target': {'now': np.nan, 'mean': 0.0, 'samples': []},
    'temperature_outside': {'now': np.nan, 'mean': 0.0, 'samples': []},
    'humidity_sht': {'now': np.nan, 'mean': 0.0, 'samples': []},
    'humidity_th': {'now': np.nan, 'mean': 0.0, 'samples': []},
    'pressure_bmp': {'now': np.nan, 'mean': 0.0, 'samples': []},
    'voc': {'now': np.nan, 'mean': 0.0, 'samples': []},
    'co2': {'now': np.nan, 'mean': 0.0, 'samples': []},
    'pow': {'state': 1},
    'mode': {'state': 0},
    'cmp': {'now': 0.0, 'mean': 0, 'samples': []},
}


def main():
    """Execute main loop."""
    global CONFIG
    global DEBUG
    global MYAPP
    global MYROOT
    global DATA_STORE
    global AIRCO
    global AIRCO_IP
    killer = ml.GracefulKiller()
    start_monitoring(seconds_frozen=anti_freeze, test_interval=1357)
    report_time = constants.AC['report_time']
    grace_period = constants.AC['gracetime']
    AIRCO_IP = constants.AC['ip_airco']
    fdatabase = constants.AC['database']
    print(f"Using database : {fdatabase}")
    sql_cmd = constants.AC['sql_command']
    total_samples = constants.AC['samplesperreport']
    cnt_dwn_samples = total_samples
    sample_time = report_time / cnt_dwn_samples
    start_time = 0

    test_db_connection(fdatabase)

    AIRCO = daikinlib.Daikin(AIRCO_IP)
    # logging will fail if airco is not present
    print(f"{AIRCO.__str__()}")

    # load state
    DATA_STORE = load_state(CONFIG, DATA_STORE)

    if not DEBUG:
        # CSS811 requires a stabilisation time after start-up
        # when we are not debugging, we wait for that here.
        time.sleep(grace_period)

    pause_time = time.time()
    while not killer.kill_now:
        if time.time() > pause_time:
            start_time = time.time()
            cnt_dwn_samples -= 1
            try:
                do_work(total_samples)
                if cnt_dwn_samples <= 0:
                    do_add_to_database(fdatabase, sql_cmd)
                    save_state()
                    cnt_dwn_samples = total_samples
            except OSError:
                if DEBUG:
                    print("OSError")
                    print(traceback.format_exc())
                    print("The above error will be passed.")
                # pass
            except Exception:
                print("Unexpected error in run()")
                print(traceback.format_exc())
                print("The above error will be raised. Please pick up the pieces.")
                raise

            pause_time = (sample_time
                          - (time.time() - start_time)
                          - (start_time % sample_time)
                          + time.time())
            if pause_time > 0 and DEBUG:
                print(f"Waiting  : {pause_time - time.time():.1f}s")
                print("................................")
            if pause_time < 0 and DEBUG:
                print(f"Behind   : {pause_time - time.time():.1f}s")
                print("................................")
        time.sleep(1.0)


def do_work(nmbr_of_samples):
    """Process the data from the website"""
    global AIRCO
    global DATA_STORE
    global DEBUG

    retries = 3
    while retries > 0:
        try:
            DATA_STORE['pow']['state'] = AIRCO.power
            break
        except ConnectionError:
            print("AC ConnectionError")
            time.sleep(10)
            retries -= 1

    retries = 3
    while retries > 0:
        try:
            DATA_STORE['mode']['state'] = AIRCO.mode
            break
        except ConnectionError:
            print("AC ConnectionError")
            time.sleep(10)
            retries -= 1

    sensor_data = dict()
    # TEMPerHUM
    sensor_data['th'] = asens.get_tmphm_data()
    for k in sensor_data['th']:
        if 'temperature' in k:
            DATA_STORE['temperature_th']['now'] = validate_sensor_data('temperature',
                                                                       k,
                                                                       DATA_STORE['temperature_th']['now'],
                                                                       sensor_data['th'][k],
                                                                       raenge=100
                                                                       )
        if 'humidity' in k:
            DATA_STORE['humidity_th']['now'] = validate_sensor_data('humidity',
                                                                    k,
                                                                    DATA_STORE['humidity_th']['now'],
                                                                    sensor_data['th'][k],
                                                                    raenge=100
                                                                    )

    # SHT31
    sensor_data['sht'] = asens.get_sht_data()
    for k in sensor_data['sht']:
        if 'temperature' in k:
            DATA_STORE['temperature_sht']['now'] = validate_sensor_data('temperature',
                                                                        k,
                                                                        DATA_STORE['temperature_sht']['now'],
                                                                        sensor_data['sht'][k],
                                                                        raenge=100
                                                                        )
        if 'humidity' in k:
            DATA_STORE['humidity_sht']['now'] = validate_sensor_data('humidity',
                                                                     k,
                                                                     DATA_STORE['humidity_sht']['now'],
                                                                     sensor_data['sht'][k],
                                                                     raenge=100
                                                                     )
    # BMP058
    sensor_data['bmp'] = asens.get_bmp_data()
    for k in sensor_data['bmp']:
        if 'temperature' in k:
            DATA_STORE['temperature_bmp']['now'] = validate_sensor_data('temperature',
                                                                        k,
                                                                        DATA_STORE['temperature_bmp']['now'],
                                                                        sensor_data['bmp'][k],
                                                                        raenge=100
                                                                        )
        if 'pressure' in k:
            DATA_STORE['pressure_bmp']['now'] = validate_sensor_data('pressure',
                                                                     k,
                                                                     DATA_STORE['pressure_bmp']['now'],
                                                                     sensor_data['bmp'][k],
                                                                     raenge=100
                                                                     )

    # CCS
    sensor_data['ccs'] = asens.get_ccs_qdata()
    for k in sensor_data['ccs']:
        if 'voc' in k:
            DATA_STORE['voc']['now'] = validate_sensor_data('voc',
                                                            k,
                                                            DATA_STORE['voc']['now'],
                                                            sensor_data['ccs'][k],
                                                            raenge=2000
                                                            )
        if 'co2' in k:
            DATA_STORE['co2']['now'] = validate_sensor_data('co2',
                                                            k,
                                                            DATA_STORE['co2']['now'],
                                                            sensor_data['ccs'][k],
                                                            raenge=2000
                                                            )

    # AC
    retries = 3
    while retries > 0:
        try:
            DATA_STORE['cmp']['now'] = AIRCO.compressor_frequency
            break
        except ConnectionError:
            print("AC ConnectionError")
            time.sleep(10)
            retries -= 1
    retries = 3
    while retries > 0:
        try:
            DATA_STORE['temperature_ac']['now'] = AIRCO.inside_temperature
            break
        except ConnectionError:
            print("AC ConnectionError")
            time.sleep(10)
            retries -= 1
    retries = 3
    while retries > 0:
        ac_tgt = 'M'
        try:
            ac_tgt = AIRCO.target_temperature
            break
        except ConnectionError:
            print("AC ConnectionError")
            time.sleep(10)
            retries -= 1
    if (ac_tgt in ['--', 'M']) or (DATA_STORE['pow']['state'] == 0):
        if DEBUG:
            print("No target temperature set.")
        DATA_STORE['temperature_target']['now'] = DATA_STORE['temperature_ac']['now']
    else:
        DATA_STORE['temperature_target']['now'] = float(ac_tgt)
    retries = 3
    while retries > 0:
        try:
            DATA_STORE['temperature_outside']['now'] = AIRCO.outside_temperature
            break
        except ConnectionError:
            print("AC ConnectionError")
            time.sleep(10)
            retries -= 1

    for k in DATA_STORE:
        try:
            DATA_STORE[k]['samples'].append(DATA_STORE[k]['now'])
            while len(DATA_STORE[k]['samples']) > nmbr_of_samples:
                DATA_STORE[k]['samples'].pop(0)
            ds_mean = round(float(np.nanmean(DATA_STORE[k]['samples'], dtype=float)), 1)
            # the mean value is stored to an accuracy of 1 decimal
            if DEBUG:
                print(f"{k} = {ds_mean} < {DATA_STORE[k]['samples']}")
            DATA_STORE[k]['mean'] = ds_mean
        except KeyError:
            pass
    if DEBUG:
        print(f"+----------------Room {constants.AC['room_id']} Data----------------")
        print(f"| T(sensors): TEMPerHUM = {DATA_STORE['temperature_th']['now']:.2f} degC")
        print(f"|             SHT031    = {DATA_STORE['temperature_sht']['now']:.2f} degC")
        print(f"|             BMP085    = {DATA_STORE['temperature_bmp']['now']:.2f} degC")
        print(f"| T(airco)  : Inside      {DATA_STORE['temperature_ac']['now']:.2f} degC "
              f"state = {DATA_STORE['pow']['state']}")
        print(f"|             Target >>>> {DATA_STORE['temperature_target']['now']:.2f} degC "
              f" mode = {DATA_STORE['mode']['state']}")
        print(f"|             Outside     {DATA_STORE['temperature_outside']['now']:.2f} degC")
        print(f"| RH        : TEMPerHUM = {DATA_STORE['humidity_th']['now']} %")
        print(f"|             SHT031    = {DATA_STORE['humidity_sht']['now']:.2f} %")
        print(f"| p         : {DATA_STORE['pressure_bmp']['now']} mbara")
        print(f"| VOC       : {DATA_STORE['voc']['now']} ppb")
        print(f"| CO2       : {DATA_STORE['co2']['now']} ppm")
        print(f"| compressor: {DATA_STORE['cmp']['samples'][-1]:.0f} ")
        print("+--------------------------------------------")


def validate_sensor_data(key_string, element, current_value, proposed_new_value, raenge=100):
    """[summary]

    Args:
        key_string (string): partial name of key that must be present
        element (float): name of key in the sensor_data

    Returns:
        bool: True is element contains a valid value
    """
    # new or old value is NaN; always accept new value
    if np.isnan(current_value) or np.isnan(proposed_new_value):
        return proposed_new_value

    factor_limit = 0.10
    return_value = np.nan
    # determine the relative difference
    factor = abs(proposed_new_value - current_value) / raenge

    if factor < factor_limit:
        return_value = proposed_new_value

    return return_value


def do_add_to_database(fdatabase, sql_cmd):
    """Commit the results to the database."""
    global DEBUG
    global DATA_STORE
    conn = None
    cursor = None

    epoch = int(dt.datetime.now().timestamp())
    date_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(epoch))

    results = np.array([date_time,
                        epoch,
                        DATA_STORE['pow']['state'],
                        DATA_STORE['mode']['state'],
                        DATA_STORE['temperature_sht']['mean'],
                        DATA_STORE['temperature_bmp']['mean'],
                        DATA_STORE['temperature_th']['mean'],
                        DATA_STORE['temperature_ac']['mean'],
                        DATA_STORE['temperature_target']['mean'],
                        DATA_STORE['temperature_outside']['mean'],
                        DATA_STORE['humidity_sht']['mean'],
                        DATA_STORE['humidity_th']['mean'],
                        DATA_STORE['pressure_bmp']['mean'],
                        DATA_STORE['voc']['mean'],
                        DATA_STORE['co2']['mean'],
                        int(round(DATA_STORE['cmp']['mean']))
                        ])

    for i, k in enumerate(results):
        if 'nan' in k:
            results[i] = "NULL"

    if DEBUG:
        print(f"   @: {results[0]}")
        print(f"    : {results[1:]}")

    retries = 6
    while retries > 0:
        try:
            conn = create_db_connection(fdatabase)
            cursor = conn.cursor()
            if not epoch_is_present_in_database(cursor, epoch):
                if DEBUG:
                    print(f">>> : {date_time} = {results}")
                cursor.execute(sql_cmd, results)
                cursor.close()
                conn.commit()
                conn.close()
                retries = -1
            else:
                retries = 0
        except sqlite3.OperationalError:
            retries -= 1
            if cursor:
                cursor.close()
            if conn:
                conn.close()
            raise
    if retries == 0 and DEBUG:
        print(f"Skip: {results[0]}")


def epoch_is_present_in_database(db_cur, epoch):
    """
    Test if results is already present in the database
    :param db_cur: object database-cursor
    :param epoch: int
    :return: boolean  (true if data is present in the database for the given site at or after the given epoch)
    """
    db_cur.execute("SELECT MAX(sample_epoch) FROM aircon;")
    db_epoch = db_cur.fetchone()[0]
    if db_epoch:
        if db_epoch >= epoch:
            return True
    return False


def create_db_connection(database_file):
    """
    Create a database connection to the SQLite3 database specified by database_file.
    """
    global DEBUG
    consql = None
    if DEBUG:
        print(f"Connecting to: {database_file}")
    try:
        consql = sqlite3.connect(database_file, timeout=9000)
        return consql
    except sqlite3.Error:
        print("Unexpected SQLite3 error when connecting to server.")
        print(traceback.format_exc())
        if consql:  # attempt to close connection to SQLite3 server
            consql.close()
            print(" ** Closed SQLite3 connection. **")
        raise


def test_db_connection(fdatabase):
    """
    Test & log database engine connection.
    """
    global DEBUG
    try:
        conn = create_db_connection(fdatabase)
        cursor = conn.cursor()
        cursor.execute("SELECT sqlite_version();")
        versql = cursor.fetchone()
        cursor.close()
        conn.commit()
        conn.close()
        print(f"Attached to SQLite3 server: {versql}")
        print(f"Using DB file             : {fdatabase}")
    except sqlite3.Error:
        print("Unexpected SQLite3 error during test.")
        print(traceback.format_exc())
        raise


def save_state():
    """Save the settings to disk."""
    global CONFIG
    global DEBUG
    nosj_data = dict()
    # first load the settings!
    if os.path.isfile(CONFIG):
        with open(CONFIG, 'r') as fp:
            nosj_data = json.load(fp)
    # keep the gain and offset, in case they were changed
    try:
        DATA_STORE['temperature']['gain'] = nosj_data['sensors']['temperature']['gain']
        DATA_STORE['temperature']['offset'] = nosj_data['sensors']['temperature']['offset']
    except KeyError:
        DATA_STORE['temperature']['gain'] = 1.0
        DATA_STORE['temperature']['offset'] = 0.0
    # then insert current telemetry
    nosj_data['sensors'] = DATA_STORE
    # update the timestamp
    nosj_data['time'] = time.time()
    # and write the file back
    if DEBUG:
        print("Saving state.")
    with open(CONFIG, 'w') as fp:
        json.dump(nosj_data, fp, sort_keys=True, indent=4)


def load_state(from_file, defaults):
    """Combine defaults with a local configurationfile (if exists)."""
    global DEBUG
    nosj_data = dict()
    if DEBUG:
        print("Loading state.")
    if os.path.isfile(from_file):
        with open(from_file, 'r') as fp:
            nosj_data = json.load(fp)
            try:
                nosj_data = nosj_data['sensors']
            except KeyError:
                nosj_data = {}
                # pass
    # add missing defaults
    for element in defaults:
        if element not in nosj_data:
            nosj_data[element] = defaults[element]
    return nosj_data


if __name__ == "__main__":
    if OPTION.debug:
        DEBUG = True
        print("Debug-mode started.")
        print("Use <Ctrl>+C to stop.")
        main()

    if OPTION.start:
        main()

    print("And it's goodnight from him")
