#!/bin/bash

# Use stop.sh to stop all daemons in one go
# You can use update.sh to get everything started again.

HERE=$(cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)

pushd "${HERE}" || exit 1
    # shellcheck disable=SC1091
    source ./bin/constants.sh

    sudo systemctl stop aircon.fles.service

    sudo systemctl stop aircon.contrlr.service
    sudo systemctl stop aircon.sensair.service

    sudo systemctl stop aircon.regress.timer
    sudo systemctl stop aircon.trend.day.timer
    sudo systemctl stop aircon.update.timer
popd || exit
